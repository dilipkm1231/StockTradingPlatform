package com.hcl.trad.Configration;


import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.hcl.trad.StockTradingPlatform.dao.StockTradingPlatformDao;
import com.hcl.trad.StockTradingPlatform.daoImpl.StockTradingPlatformDaoImpl;
import com.hcl.trad.StockTradingPlatform.service.StockTradingPlatformService;
import com.hcl.trad.StockTradingPlatform.serviceImpl.StockTradingPlatformServiceImpl;
import com.hcl.trad.model.StockTrade;

@Configuration
@ConditionalOnClass(DataSource.class)
@ComponentScan(basePackages = "com.hcl.trad.*")
@PropertySource(value = {"classpath:application.properties"})
public class ApplicationConfig {
	@Autowired
	Environment env;
	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(env.getRequiredProperty("spring.datasource.driverClassName"));
		dataSource.setUrl(env.getRequiredProperty("spring.datasource.url"));
		dataSource.setUsername(env.getRequiredProperty("spring.datasource.username"));
		dataSource.setPassword(env.getRequiredProperty("spring.datasource.password"));
		return dataSource;
	}
	@Bean
	public JdbcTemplate jdbcTemplate(DataSource dataSource) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.setResultsMapCaseInsensitive(true);
		return jdbcTemplate;
		
	}
	@Bean
	public StockTrade stockTrade() {
		StockTrade stockTrade = new StockTrade();
		return stockTrade;
		
	}
	
	@Bean
	public StockTradingPlatformService stockTradingPlatformService() {
		StockTradingPlatformService stockTradingPlatformService = new StockTradingPlatformServiceImpl();
		return stockTradingPlatformService;
		
	}
	
	@Bean
	public StockTradingPlatformDao stockTradingPlatformDao() {
		StockTradingPlatformDao stockTradeDao = new StockTradingPlatformDaoImpl();
		return stockTradeDao;
		
	}
}
