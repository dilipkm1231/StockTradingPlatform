package com.hcl.trad.StockTradeControllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.trad.StockTradingPlatform.service.StockTradingPlatformService;
import com.hcl.trad.model.StockTrade;

@RestController
@CrossOrigin("*")
public class StockTradeController {
	@Autowired
	StockTradingPlatformService stockTradingPlatformService;
	@RequestMapping(value = "/placeOrder", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.ALL_VALUE)
	public ResponseEntity<StockTrade> TradeStock(@RequestBody StockTrade stockTrade){
		if(stockTrade.getType() != null && stockTrade.getSymbol() != null && stockTrade.getQuantity() != null && stockTrade.getPrice() != null) {
			stockTradingPlatformService.tradeStock(stockTrade);
		}
		
		return new ResponseEntity<>(stockTrade,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/stockReport/{userID}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.ALL_VALUE)
	public ResponseEntity<List<Map<String, Object>>> stockReport(@PathVariable("userID") String userID){
	System.out.println("inside stockRepoet");
	List<Map<String, Object>> stockReport= stockTradingPlatformService.stockReport(userID);
	return new ResponseEntity<>(stockReport,HttpStatus.OK);
	}
	
	@RequestMapping("/currentStatus/{user}")
	public ResponseEntity<Map<String,Object>> currentStatus(@PathVariable("user") String userID){
		//System.out.println(userID);
		List<Map<String, Object>> stockReport= stockTradingPlatformService.stockReport(userID);
		List<Map<String,String>> currentStatusList = new ArrayList<Map<String,String>>(); 
		for(Map<String, Object> map: stockReport) {
			Map<String,String> mockObj = new HashMap<String,String>();
			mockObj.put("userId", map.get("user_id")+"");
			mockObj.put("Stock", map.get("stock")+"");
			mockObj.put("Transaction_Type", map.get("transaction_type")+"");
			mockObj.put("Qty", map.get("quantity")+"");
			mockObj.put("Price", map.get("price")+"");
			mockObj.put("Total_Amt", map.get("total_amount")+"");
			mockObj.put("Status", map.get("status")+"");
			
			currentStatusList.add(mockObj);
		}
		
		Map<String,Object> returnJSON = new HashMap<String,Object>();
		
		returnJSON.put("response", currentStatusList);
		return new ResponseEntity<>(returnJSON,HttpStatus.OK);
	}
}
