package com.hcl.trad.StockTradingPlatform.service;

import java.util.List;
import java.util.Map;

import com.hcl.trad.model.StockTrade;

public interface StockTradingPlatformService {

	public void tradeStock(StockTrade strocktrade);
	public List<Map<String, Object>> stockReport(String userID);

}
