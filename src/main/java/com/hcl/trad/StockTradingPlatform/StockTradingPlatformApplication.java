package com.hcl.trad.StockTradingPlatform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication

@ComponentScan(basePackages = "com.hcl.trad.*")
public class StockTradingPlatformApplication {

	public static void main(String[] args) {
		SpringApplication.run(StockTradingPlatformApplication.class, args);
	}
}
