package com.hcl.trad.StockTradingPlatform.serviceImpl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.trad.StockTradingPlatform.dao.StockTradingPlatformDao;
import com.hcl.trad.StockTradingPlatform.service.StockTradingPlatformService;
import com.hcl.trad.model.StockTrade;

@Service
public class StockTradingPlatformServiceImpl implements StockTradingPlatformService{
	
	@Autowired
	StockTradingPlatformDao stockTradingPlatformDao; 
	
	public void tradeStock(StockTrade strocktrade){
		List<Map<String,Object>> byTrade = stockTradingPlatformDao.getByStock(strocktrade.getSymbol());
		boolean inside =false;
		if(byTrade.isEmpty()) {
			inside = true;
			stockTradingPlatformDao.tradeStock(strocktrade,"processed");
		}else {
			
			//Case of sell
			if(strocktrade.getType().equalsIgnoreCase("SELL")) {
				for(Map<String,Object> currentMap: byTrade) {
					if((currentMap.get("transaction_type")+"").equalsIgnoreCase("BUY")) {
						
						if(((Double)currentMap.get("price")) >= strocktrade.getPrice()) {
							if(((Integer)currentMap.get("quantity")) >= strocktrade.getQuantity()) { // SEll all Status == executed
								stockTradingPlatformDao.tradeStock(strocktrade,"executed");
								if(((Integer)currentMap.get("quantity")) == strocktrade.getQuantity()) {
									stockTradingPlatformDao.updateBuyerORSeller((Integer)currentMap.get("stock_trade_id"), (Integer)currentMap.get("quantity"), "executed");
									inside =true;
									break;
								}else {
									
									int remainingQuantity = ((Integer)currentMap.get("quantity")) - strocktrade.getQuantity();
									stockTradingPlatformDao.updateBuyerORSeller((Integer)currentMap.get("stock_trade_id"), remainingQuantity, "processed");
									inside =true;
									break;
								}
								
								//update if value of buyer is greater than seller
								
							}else { // half executed logic below
							
									strocktrade.setQuantity(strocktrade.getQuantity()-((Integer)currentMap.get("quantity")));
									stockTradingPlatformDao.tradeStock(strocktrade,"processed");
									stockTradingPlatformDao.updateBuyerORSeller((Integer)currentMap.get("stock_trade_id"), (Integer)currentMap.get("quantity"), "executed");
									inside =true;
									break;
									
								
							}
						}
					}
					
				}
			}else {
				//case of buy
				for(Map<String,Object> currentMap: byTrade) {
					if((currentMap.get("transaction_type")+"").equalsIgnoreCase("SELL")) {
						if(((Double)currentMap.get("price")) <= strocktrade.getPrice()) {
							if(((Integer)currentMap.get("quantity")) <= strocktrade.getQuantity()) {
								stockTradingPlatformDao.tradeStock(strocktrade,"executed");
							if(((Integer)currentMap.get("quantity")) == strocktrade.getQuantity()) {
								stockTradingPlatformDao.updateBuyerORSeller((Integer)currentMap.get("stock_trade_id"), (Integer)currentMap.get("quantity"), "executed");
								inside =true;
								break;
							}else {
								
								int remainingQuantity = ((Integer)currentMap.get("quantity")) - strocktrade.getQuantity();
								stockTradingPlatformDao.updateBuyerORSeller((Integer)currentMap.get("stock_trade_id"), remainingQuantity, "processed");
								inside =true;
								break;
							}
								
							}else { // half executed logic below
							
									strocktrade.setQuantity(strocktrade.getQuantity()-((Integer)currentMap.get("quantity")));
									stockTradingPlatformDao.tradeStock(strocktrade,"processed");
									stockTradingPlatformDao.updateBuyerORSeller((Integer)currentMap.get("stock_trade_id"), (Integer)currentMap.get("quantity"), "executed");
									inside =true;
									break;
									
								
							}
						}
					}
				}
				//stockTradingPlatformDao.tradeStock(strocktrade,"processed");
			}
			
		}
		if(!inside) {
			stockTradingPlatformDao.tradeStock(strocktrade,"processed");
		}
		//stockTradingPlatformDao.tradeStock(strocktrade);
		
	}
	
	@Override
	public List<Map<String, Object>> stockReport(String userID) {
	List<Map<String, Object>> stockReport =stockTradingPlatformDao.stockReport(userID);
	return stockReport;
	}

}
