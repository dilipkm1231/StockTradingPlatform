package com.hcl.trad.StockTradingPlatform.dao;

import java.util.List;
import java.util.Map;

import com.hcl.trad.model.StockTrade;

public interface StockTradingPlatformDao {
	
	public void tradeStock(StockTrade strocktrade,String status);
	
	public List<Map<String,Object>> getByStock(String stock);
	
	public void updateBuyerORSeller(Integer userId,Integer quantity, String status);
	
	public List<Map<String, Object>> stockReport(String userID);

}
