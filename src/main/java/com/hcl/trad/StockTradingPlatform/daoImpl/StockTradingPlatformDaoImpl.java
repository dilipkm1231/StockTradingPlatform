package com.hcl.trad.StockTradingPlatform.daoImpl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.hcl.trad.StockTradingPlatform.dao.StockTradingPlatformDao;
import com.hcl.trad.model.StockTrade;

@Repository
public class StockTradingPlatformDaoImpl implements StockTradingPlatformDao {

	@Autowired
	JdbcTemplate jdbcTemplate;

	private final String Insertsql = "INSERT INTO stock_trade (user_id, stock, transaction_type, quantity, price,total_amount,status,created_date) values(?,?,?,?,?,?,?,?) ";
	private final String selectsql = "SELECT stock_trade_id,user_id, stock, transaction_type, quantity, price,total_amount,status,created_date FROM stock_trade WHERE stock =?";
	private final String updatesql = "UPDATE stock_trade SET quantity =?, status =? WHERE stock_trade_id =?";
	private final String fetchStockReport= "Select user_id, stock, transaction_type, quantity, price,total_amount,status,created_date from stock_trade where user_id=?";
	@Override
	public void tradeStock(StockTrade strocktrade, String status) {
		Double total_amount = strocktrade.getPrice() * strocktrade.getQuantity();
		SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy/mm/dd");
		jdbcTemplate.update(Insertsql, strocktrade.getUserId(), strocktrade.getSymbol(), strocktrade.getType(),
				strocktrade.getQuantity(), strocktrade.getPrice(), total_amount, status, simpleDate.format(new Date()));

	}

	@Override
	public List<Map<String, Object>> getByStock(String stock) {
		List<Map<String, Object>> map = jdbcTemplate.queryForList(selectsql, stock);
		return map;
	}

	@Override
	public void updateBuyerORSeller(Integer stockTradeID, Integer quantity, String status) {
		jdbcTemplate.update(updatesql, quantity, status, stockTradeID);
	}
	
	@Override
	public List<Map<String, Object>> stockReport(String userID) {
	List<Map<String, Object>> stockReport= jdbcTemplate.queryForList(fetchStockReport, userID);
	return stockReport;
	}

}
