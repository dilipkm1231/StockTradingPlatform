create database StockTradingPlatform;

CREATE TABLE `stocktradingplatform`.`stock_trade` (
  `stock_trade_id` INT NOT NULL AUTO_INCREMENT,
  `user_id` VARCHAR(45) NOT NULL,
  `stock` VARCHAR(45) NOT NULL,
  `transaction_type` VARCHAR(45) NOT NULL,
  `quantity` INT NOT NULL,
  `price` INT NOT NULL,
  `total_amount` INT NOT NULL,
  `status` VARCHAR(45) NOT NULL,
  `created_date` DATETIME NOT NULL,
  PRIMARY KEY (`stock_trade_id`));
